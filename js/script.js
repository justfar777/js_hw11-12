// 1 - Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ

const one = document.getElementById("firsttask")
console.log(`Відступ на сторінці у блока ${getComputedStyle(one)["margin"]} зі всіх сторін`)

// 2 - Створіть програму секундомір.
// 	- Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// 	- При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
// 	- Виведення лічильників у форматі ЧЧ:ММ:СС
// 	- Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

const hourElement = document.querySelector('.hour'),
	minuteElement = document.querySelector('.minute'),
	secondElement = document.querySelector('.second'),
	startButton = document.querySelector('.start'),
	stopButton = document.querySelector('.stop'),
	resetButton = document.querySelector('.reset'),
	colorBlack = document.querySelector('.black');

startButton.addEventListener('click', () => {
	clearInterval(interval)
	interval = setInterval(startTimer, 1000)
	colorBlack.classList.add('green')
	colorBlack.classList.remove('black')
	colorBlack.classList.remove('silver')
	colorBlack.classList.remove('red')
})

stopButton.addEventListener('click', () => {
	clearInterval(interval)
	colorBlack.classList.add('red')
	colorBlack.classList.remove('green')
	colorBlack.classList.remove('silver')
})

resetButton.addEventListener('click', () => {
	clearInterval(interval)
	hour = 00
	minute = 00
	second = 00
	hourElement.textContent = "00"
	minuteElement.textContent = "00"
	secondElement.textContent = "00"
	colorBlack.classList.add('silver')
	colorBlack.classList.remove('red')
	colorBlack.classList.remove('green')
})

let hour = 00,
	minute = 00,
	second = 00,
	interval;

function startTimer() {
	second++
	if (second < 10) {
		secondElement.innerText = "0" + second
	}
	if (second > 9) {
		secondElement.innerText = second
	}
	if (second > 59) {
		minute++
		minuteElement.innerText = "0" + minute
		second = 0
		secondElement.innerText = "0" + second
	}
	if (minute < 10) {
		minuteElement.innerText = "0" + minute
	}
	if (minute > 9) {
		minuteElement.innerText = minute
	}
	if (minute > 59) {
		hour++
		hourElement.innerText = "0" + hour
		minute = 0
		minuteElement.innerText = "0" + minute
	}
	if (hour < 10) {
		hourElement.innerText = "0" + hour
	}
	if (hour > 9) {
		hourElement.innerText = hour
	}
	if (hour > 60) {
		hourElement.innerText = "00"
		minuteElement.innerText = "00"
		secondElement.innerText = "00"
	}
}


// 3- Реалізуйте програму перевірки телефону
//  - Використовуючи JS Створіть поле для введення телефону та кнопку збереження
//  - Користувач повинен ввести номер телефону у форматі 000-000-00-00
//  - Після того як користувач натискає кнопку зберегти перевірте правильність введення номера,
//    якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку
// 	https://www.meme-arsenal.com/memes/bedfc9ca010f2d5f866e9de5171bfb1f.jpg якщо буде помилка, відобразіть її в діві до input.


const messageInput = document.createElement('div')
messageInput.innerText = "Введіть номер телефону"
messageInput.classList.add('messageinput')
const inputArea = document.createElement('input')
inputArea.setAttribute('type', 'text')
inputArea.setAttribute('placeholder', '000-000-00-00')
inputArea.classList.add('inputarea')
const containerInput = document.getElementById('thirdtask')
const inputButton = document.createElement('button')
inputButton.innerText = 'Зберігти'
inputButton.classList.add('inputbtn')

containerInput.appendChild(messageInput)
containerInput.appendChild(inputArea)
containerInput.appendChild(inputButton)

inputButton.addEventListener('click', () => {
	let numTel = inputArea.value
	let numTel_pattern = /^\d{3} \d{3} \d{2} \d{2}$/
	let check = numTel_pattern.test(numTel)
	if (check == true) {
		inputArea.classList.add('green')
		setTimeout(() => {
			document.location = 'https://www.meme-arsenal.com/memes/bedfc9ca010f2d5f866e9de5171bfb1f.jpg'
		}, 1500)
	}
	else {
		inputArea.classList.add('red')
		let = alertMessage = document.createElement('div')
		alertMessage.innerText = 'Невірно введений номер або формат'
		alertMessage.classList.add('alertmessage')
		containerInput.appendChild(alertMessage)
	}
})


// 4 - Слайдер
// - Створіть слайдер кожні 3 сек змінюватиме зображення

const images = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg', '9.jpg', '10.jpg', '11.jpg', '12.jpg', '13.jpg', '14.jpg', '15.jpg', '16.jpg',];
const slider = document.getElementById('slider');
const img = slider.querySelector('img');

let i = 1;
img.src = 'img/' + images[0];

window.setInterval(function () {
	img.src = 'img/' + images[i]
	i++
	if (i == images.length) {
		i = 0
	}
}, 3000)